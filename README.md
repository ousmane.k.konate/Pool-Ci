# Time Registration

The goal of this project is to create a time manager tool for employees and their managers.

Users will be able to:<br>
• Change their account information.<br>
• Delete his account.<br>
• Report their departure and arrival times.<br>
• See their dashboards.<br>

The managers and the general manager will be able to:<br>
• Manage their team(s).<br>
• View the team's average daily and weekly hours over a given period.<br>
• View an employee's daily and weekly working hours over a period of time.<br>
• View dashboards of their employees.<br>

The Director General may:<br>
• Promote a user from employee to manager.<br>
• Display the dashboard in order to fall users.<br>
• Delete all user accounts.<br>

<h3> Screenshot </h3>

<img width="auto" alt="Capture d’écran 2022-11-09 à 12 05 11" src="https://user-images.githubusercontent.com/90601503/200814563-5725c84b-4ee0-4f58-bae2-e42af7d3372d.png">

# Installation

Command :<br>
• npm install <br>
• npm run serve <br>

# Technology used

• Elixir & Phoenix <br>
• Vue.js <br>
• Docker <br>
• Gitlab CI <br>
• Bootstrap <br>
