import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import './axios'
import { BootstrapVue3 } from 'bootstrap-vue-3'


// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'



import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons' // fas == font awesome solid
import { far } from '@fortawesome/free-regular-svg-icons' // far == ''''''' regular
import { fab } from '@fortawesome/free-brands-svg-icons' // fab == ''''''' brand
import route from "./route.js"

library.add(fas, far, fab)

createApp(App)
    .use(BootstrapVue3)
    .component('font-awesome-icon', FontAwesomeIcon)
    .use(store).use(route).mount('#app')
