import { createRouter, createWebHistory } from 'vue-router'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Profil from './components/Profil.vue'
import Times from './components/Times.vue'
import Home from './components/Home.vue'

const route = createRouter({
    history: createWebHistory(),
    routes:[
        {path: '/login', component: Login, name: 'Login'},
        {path: '/register', component: Register, name: 'Register'},
        // {path: '/forgot', component: Forgot, name: 'Forgot'},
        {path: '/', component: Home, name: 'Home'},
        {path: '/profil', component: Profil, name: 'Profil'},
        {path: '/times', component: Times, name: 'Times'},
    ]
});

export default route;