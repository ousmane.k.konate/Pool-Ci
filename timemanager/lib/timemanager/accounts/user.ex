defmodule Timemanager.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :username, :string
    field :password, :string
    field :role, Ecto.Enum, values: [:admin, :employee]

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :password, :role])
    |> validate_required([:username, :email, :role])
    |> validate_format(:email, ~r/@/)
    |> validate_length(:username, min: 2, max: 40)
    |> update_change(:email, fn email -> String.downcase(email) end)
    |> unique_constraint(:email)
  end

  def registration_changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :password, :role])
    |> validate_required([:username, :email, :password, :role])
    |> validate_format(:email, ~r/@/)
    |> update_change(:email, fn email -> String.downcase(email) end)
    |> unique_constraint(:email)
    |> encrypt_password()
  end

  defp encrypt_password(user) do
    with password <- fetch_field!(user, :password) do
      encrypted_password = Bcrypt.hash_pwd_salt(password)
      put_change(user, :password, encrypted_password)
    end
  end
end
