defmodule TimemanagerWeb.AuthenticationPlug do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _) do
    token = authorization_header = get_req_header(conn, "authorization") |> List.first()
    if token == nil do
      send_resp(conn, :unauthorized, "")
    else
      token = String.replace(token, "Bearer ", "")
      signer = Joken.Signer.create("HS256", System.get_env("JWT_SECRET"))
      with {:ok, claims} <- TimemanagerWeb.JWTAuthentication.verify_and_validate(token, signer) do
        conn |> put_status(200)
      else
        {:error, _reason} -> send_resp(conn, :unauthorized, "")
      end
    end
  end
end
