defmodule TimemanagerWeb.AuthenticationController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger

  alias Timemanager.Accounts
  alias Timemanager.Accounts.User
  alias TimemanagerWeb.JWTAuthentication

  action_fallback TimemanagerWeb.FallbackController

  def swagger_definitions do
    %{
      Signin: swagger_schema do
        title "Signin"
        description "Log in informations"
        properties do
          email :string, "User's email address", required: true
          password :string, "User's password", required: true
        end
        example %{
          email: "toto@epitech.eu",
          password: "pwd"
        }
      end,
      Signup: swagger_schema do
        title "Signup"
        description "Register informations"
        properties do
          username :string, "Username", required: true
          email :string, "User's email address", required: true
          role :string, "User's role", required: true
          password :string, "User's password", required: true
        end
        example %{
          username: "toto",
          password: "pwd",
          email: "toto@epitech.eu",
          role: "admin"
        }
      end,
      RegisteredUser: swagger_schema do
        title "RegisteredUser"
        description "Register informations returned"
        properties do
          username :string, "Username", required: true
          email :string, "User's email address", required: true
          role :string, "User's role", required: true
        end
        example %{
          username: "toto",
          email: "toto@epitech.eu",
          role: "admin"
        }
      end,
      LoggedUser: swagger_schema do
        title "LoggedUser"
        description "User logged successfully"
        properties do
          id :integer, "User id", required: true
          access_token :string, "JWT access token", required: true
          role :string, "User's role", required: true
          message :string, "message about user logged successfully", required: true
        end
        example %{
          id: 1,
          access_token: "eyJhbGciOiJIUusroqgihgqnurgzfO.bdnidudzysfisnv",
          role: "admin",
          message: "User successfully authenticated."
        }
      end
    }
  end

  swagger_path :sign_up_user do
    post "/auth/sign_up"
    summary "Register a user"
    description "Register a user"
    produces "application/json"
    tag "Auth"
    parameters do
      user :body, Schema.ref(:Signup), "User attributes to register"
    end
    response 201, "Created", Schema.ref(:RegisteredUser)
  end

    def sign_up_user(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.register_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show, user))
      |> render("signup.json", user: user)
    end
  end

  swagger_path :sign_in_user do
    post "/auth/sign_in"
    summary "Log a user"
    description "Log a user"
    produces "application/json"
    tag "Auth"
    parameters do
      user :body, Schema.ref(:Signin), "User attributes to register"
    end
    response 200, "", Schema.ref(:LoggedUser)
  end

  def sign_in_user(conn, %{"email" => email, "password" => password}) do
    with {:ok, user} <- Accounts.authenticate_user(email, password) do
      signer = Joken.Signer.create("HS256", System.get_env("JWT_SECRET"))
      cxsrf_token = Phoenix.Controller.get_csrf_token()
      token_extra_claims = %{user_id: user.id, user_role: user.role, cxsrf_token: cxsrf_token}
      {:ok, token, _claims} = JWTAuthentication.generate_and_sign(token_extra_claims, signer)
      conn = Plug.Conn.put_resp_cookie(conn, "c-xsrf-token", cxsrf_token)
      render(conn, "signin.json", user: user, token: token)
    else {:error, error} -> send_resp(conn, error, "")
    end
  end

  swagger_path :sign_out_user do
    post "/auth/sign_out"
    summary "Log out user"
    description "Log out user"
    produces "application/json"
    tag "Auth"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    response 204, "No-Content"
  end

  def sign_out_user(conn, %{}) do
    Phoenix.Controller.delete_csrf_token()
    send_resp(conn, :no_content, "")
  end

  def validate_token(conn, %{"token" => token}) do
    JWTAuthentication.verify_and_validate()
  end
end
