defmodule TimemanagerWeb.ClockController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger

  alias Timemanager.Clocks
  alias Timemanager.Clocks.Clock

  action_fallback TimemanagerWeb.FallbackController

  def swagger_definitions do
    %{
      Clock: swagger_schema do
        title "Clock"
        description "Clock"
        properties do
          time :datetime, "Date", required: true
          status :boolean, "Status", required: true
        end
        example %{
          time: "2000-01-01 23:00:07",
          status: true
        }
      end,
      ClockWithId: swagger_schema do
        title "ClockWithId"
        description "A user of the application"
        properties do
          id :integer
          time :datetime, "Date", required: true
          status :boolean, "Status", required: true
        end
        example %{
          id: 123,
          time: "2000-01-01 23:00:07",
          status: true
        }
      end
    }
  end

  swagger_path :create_by_user_id do
    post "/api/clocks/{user_id}"
    summary "Create a clock"
    description "Create a clock"
    produces "application/json"
    tag "Clocks"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User's id", example: "123"
      clock :body, Schema.ref(:Clock), "Clock details"
    end
    response 201, "Created", Schema.ref(:RegisteredUser)
  end

  def create_by_user_id(conn, %{"userID" => user_id, "clock" => clock_params}) do
    clock_params = Map.put(clock_params, "user", user_id)
    with {:ok, %Clock{} = clock} <- Clocks.create_clock(clock_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.clock_path(conn, :show, clock))
      |> render("show.json", clock: clock)
    end
  end

  swagger_path :show do
    get "/api/clocks/{user_id}"
    summary "Get clock by user id"
    description "Get clock by user id"
    produces "application/json"
    tag "Clocks"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User's id", example: "123"
    end
    response 200, "OK", Schema.ref(:ClockWithId)
    response 401, "Unauthorized"
    response 404, "Clock not found"
  end

  def show(conn, %{"userID" => user_id}) do
    clocks = Clocks.get_clock_by_user_id(user_id)
    render(conn, "index.json", clocks: clocks)
  end
end
