defmodule TimemanagerWeb.UserController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger

  alias Timemanager.Accounts
  alias Timemanager.Accounts.User

  action_fallback TimemanagerWeb.FallbackController

  def swagger_definitions do
    %{
      User: swagger_schema do
        title "User"
        description "A user of the application"
        properties do
          username :string, "Username", required: true
          id :integer, "Unique id"
          email :string, "User's email address", required: true
          role :string, "User's role", required: true
          password :string, "User's password", required: true
        end
        example %{
          username: "toto",
          id: 123,
          email: "toto@epitech.eu",
          role: "admin"
        }
      end,
      Users: swagger_schema do
        title "Users"
        description "A collection of Users"
        type :array
        items Schema.ref(:User)
      end,
      UserWithoutPassword: swagger_schema do
        title "UserWithoutPassword"
        description "A user of the application"
        properties do
          username :string, "Username", required: true
          email :string, "User's email address", required: true
          role :string, "User's role", required: true
        end
        example %{
          username: "toto",
          id: 123,
          email: "toto@epitech.eu",
          role: "admin"
        }
      end
    }
  end

  swagger_path :index do
    get "/api/users"
    summary "Get users informations"
    description "Get informations about all users or about only one by using the email and username parameters (both parameters required if using them)"
    produces "application/json"
    tag "Users"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      email :query, :string, "User's email", example: "toto@epitech.eu"
      username :query, :string, "User's username", example: "toto"
    end
    response 200, "OK", Schema.ref(:User)
    response 401, "Unauthorized"
    response 404, "No user found"
  end

  def index(conn, %{"email" => email, "username" => username}) do
    users = Accounts.get_users_by_mail_and_username(email, username)
    render(conn, "index.json", users: users)
  end

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.json", users: users)
  end

  swagger_path :create do
    post "/api/users"
    summary "Create a new user"
    description "Create a new user"
    produces "application/json"
    tag "Users"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user :body, Schema.ref(:User), "User attributes"
    end
    response 201, "Created", Schema.ref(:UserWithoutPassword)
    response 401, "Unauthorized"
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Accounts.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show, user))
      |> render("show.json", users: user)
    end
  end

  swagger_path :show do
    get "/api/users/{user_id}"
    summary "Get user by id"
    description "Get informations about a user with his id"
    produces "application/json"
    tag "Users"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User's id", example: "123"
    end
    response 200, "OK", Schema.ref(:User)
    response 401, "Unauthorized"
    response 404, "User not found"
  end

  def show(conn, %{"userID" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.json", user: user)
  end

  swagger_path :update do
    put "/api/users/{user_id}"
    summary "Modify user's informations"
    description "Modify informations of a user by id"
    produces "application/json"
    tag "Users"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User's id", example: "123"
      user :body, Schema.ref(:User), "User attributes"
    end
    response 204, "Updated", Schema.ref(:UserWithoutPassword)
    response 401, "Unauthorized"
  end

  def update(conn, %{"userID" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{} = user} <- Accounts.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  swagger_path :delete_user do
    delete "/api/users/{user_id}"
    summary "Delete a user"
    description "Delete a user by id"
    produces "application/json"
    tag "Users"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User's id", example: "123"
    end
    response 204, "Deleted", Schema.ref(:UserWithoutPassword)
    response 401, "Unauthorized"
  end

  def delete_user(conn, %{"userID" => id}) do
    user = Accounts.get_user!(id)

    with {:ok, %User{}} <- Accounts.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
