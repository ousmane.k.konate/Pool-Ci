defmodule TimemanagerWeb.WorkingtimeController do
  use TimemanagerWeb, :controller
  use PhoenixSwagger

  alias Timemanager.Workingtimes
  alias Timemanager.Workingtimes.Workingtime

  action_fallback TimemanagerWeb.FallbackController

  def swagger_definitions do
    %{
      Workingtime: swagger_schema do
        title "Workingtime"
        description "Working times"
        properties do
          start :datetime, "Start date", required: true
          end_date :datetime, "Real field name : end, for end date", required: true
        end
        example %{
          start: "2000-01-01 23:00:07",
          end_date: "2000-02-01 23:00:07"
        }
      end,
      WorkingTimeWithId: swagger_schema do
        title "ClockWithId"
        description "A user of the application"
        properties do
          id :integer, "Working time id"
          start :datetime, "Start date", required: true
          end_date :datetime, "Real field name : end, for end date", required: true
        end
        example %{
          id: 123,
          start: "2000-01-01 23:00:07",
          end_date: "2000-02-01 23:00:07"
        }
      end,
      WorkingTimes: swagger_schema do
        title "WorkingTimes"
        description "A collection of Working times"
        type :array
        items Schema.ref(:WorkingTimeWithId)
      end
    }
  end

  swagger_path :index do
    get "/api/workingtimes"
    summary "Get all working times"
    description "Get all working times"
    produces "application/json"
    tag "Workingtimes"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    response 200, "OK", Schema.ref(:WorkingTimes)
    response 401, "Unauthorized"
    response 404, "No working times"
  end

  def index(conn, _params) do
    workingtimes = Workingtimes.list_workingtimes()
    render(conn, "index.json", workingtimes: workingtimes)
  end

  swagger_path :get_working_time_by_id_and_user_id do
    get "/api/workingtimes/{user_id}/{id}"
    summary "Get working time"
    description "Get working time by user id and working time id"
    produces "application/json"
    tag "Workingtimes"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User id", example: "123"
      id :path, :string, "Working time id", example: "1"
    end
    response 200, "OK", Schema.ref(:WorkingTimeWithId)
    response 401, "Unauthorized"
    response 404, "No working time found for this working time id and user id"
  end

  def get_working_time_by_id_and_user_id(conn, %{"id" => id, "userID" => user_id}) do
    workingtimes = Workingtimes.get_workingtime_by_id_and_user_id(user_id, id)
    render(conn, "index.json", workingtimes: workingtimes)
  end

  swagger_path :create_by_user_id do
    post "/api/workingtimes/{user_id}"
    summary "Create working time"
    description "Create working time by user id"
    produces "application/json"
    tag "Workingtimes"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User id", example: "123"
      workingtime :body, Schema.ref(:Workingtime), "Working time attributes"
    end
    response 201, "Created", Schema.ref(:WorkingTimeWithId)
    response 401, "Unauthorized"
  end

  def create_by_user_id(conn, %{"userID" => user_id, "workingtime" => workingtime_params}) do
    workingtime_params = Map.put(workingtime_params, "user", user_id)
    with {:ok, %Workingtime{} = workingtime} <- Workingtimes.create_workingtime(workingtime_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.workingtime_path(conn, :show, workingtime))
      |> render("show.json", workingtime: workingtime)
    end
  end

  swagger_path :show do
    get "/api/workingtimes/{user_id}"
    summary "Get working time by user id"
    description "Get working time by user id or by user id and start and end date"
    produces "application/json"
    tag "Workingtimes"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      user_id :path, :string, "User's id", example: "123"
      start :query, :string, "start date, not mandatory but need the end parameter to be used", example: "2000-01-01 23:00:07"
      end_date :query, :string, "real query parameter is end for end date, not mandatory but need the start parameter to be used", example: "2000-01-01 23:00:07"
    end
    response 200, "OK", Schema.ref(:WorkingTimeWithId)
    response 401, "Unauthorized"
    response 404, "User not found"
  end

  def show(conn, %{"id" => user_id, "start" => start_date, "end" => end_date}) do
    workingtimes = Workingtimes.get_workingtimes_by_user_id_and_dates(user_id, start_date, end_date)
    render(conn, "index.json", workingtimes: workingtimes)
  end

  def show(conn, %{"id" => user_id}) do
    workingtimes = Workingtimes.get_workingtimes_by_user_id(user_id)
    render(conn, "index.json", workingtimes: workingtimes)
  end

  def show(conn, %{"id" => id}) do
    workingtime = Workingtimes.get_workingtime(id)
    render(conn, "show.json", workingtime: workingtime)
  end

  swagger_path :update do
    put "/api/workingtimes/{id}"
    summary "Modify working time"
    description "Modify informations about a working time by its id"
    produces "application/json"
    tag "Workingtimes"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      id :path, :string, "Working time id", example: "123"
      workingtime :body, Schema.ref(:Workingtime), "Working time attributes"
    end
    response 204, "Updated", Schema.ref(:WorkingTimeWithId)
    response 401, "Unauthorized"
  end

  def update(conn, %{"id" => id, "workingtime" => workingtime_params}) do
    workingtime = Workingtimes.get_workingtime!(id)

    with {:ok, %Workingtime{} = workingtime} <- Workingtimes.update_workingtime(workingtime, workingtime_params) do
      render(conn, "show.json", workingtime: workingtime)
    end
  end

  swagger_path :delete_workingtime do
    delete "/api/workingtimes/{id}"
    summary "Delete working time"
    description "Delete working time by its id"
    produces "application/json"
    tag "Workingtimes"
    parameter("Authorization", :header, :string, "JWT access token", required: true)
    parameters do
      id :path, :string, "Working time id", example: "123"
    end
    response 204, "Deleted"
    response 401, "Unauthorized"
  end

  def delete_workingtime(conn, %{"id" => id}) do
    workingtime = Workingtimes.get_workingtime!(id)

    with {:ok, %Workingtime{}} <- Workingtimes.delete_workingtime(workingtime) do
      send_resp(conn, :no_content, "")
    end
  end
end
