defmodule TimemanagerWeb.Router do
  use TimemanagerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {TimemanagerWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug TimemanagerWeb.AuthenticationPlug
  end

  scope "/auth", TimemanagerWeb do
    pipe_through :api

    #Authentication routing
    post "/sign_in", AuthenticationController, :sign_in_user
    post "/sign_up", AuthenticationController, :sign_up_user
    post "/sign_out", AuthenticationController, :sign_out_user
  end

  scope "/api", TimemanagerWeb do
    pipe_through :api

      #Users routing
      resources "/users", UserController, except: [:new, :edit, :delete], param: "userID"
      delete "/users/:userID", UserController, :delete_user

      #Clocks routing
      get "/clocks/:userID", ClockController, :show
      post "/clocks/:userID", ClockController, :create_by_user_id

      #Workingtimes routing
      resources "/workingtimes", WorkingtimeController, except: [:new, :edit, :create, :delete]
      post "/workingtimes/:userID", WorkingtimeController, :create_by_user_id
      get "/workingtimes/:userID/:id", WorkingtimeController, :get_working_time_by_id_and_user_id
      get "/workingtimes/:userID", WorkingtimeController, :get_by_user_id_and_start_and_end_date
      delete "/workingtimes/:id", WorkingtimeController, :delete_workingtime
    end

  scope "/api/swagger" do
      forward "/", PhoenixSwagger.Plug.SwaggerUI, otp_app: :timemanager, swagger_file: "swagger.json"
    end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Timemanager back-end"
      }
    }
  end
end
