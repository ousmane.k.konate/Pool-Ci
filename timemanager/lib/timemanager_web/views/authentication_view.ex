defmodule TimemanagerWeb.AuthenticationView do
  use TimemanagerWeb, :view
  alias TimemanagerWeb.AuthenticationView

  def render("signup.json", %{user: user}) do
    %{
      message: "User successfully registered.",
      id: user.id,
    }
  end

  def render("signin.json", %{user: user, token: token}) do
    %{
      message: "User successfully authenticated.",
      id: user.id,
      role: user.role,
      access_token: token
    }
  end
end
