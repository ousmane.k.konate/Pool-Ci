defmodule Timemanager.Repo.Migrations.AddRoleColumnToUserTable do
  use Ecto.Migration

  def change do
    query = "CREATE TYPE user_role AS ENUM ('admin', 'employee')"
    execute(query)

    alter table(:users) do
      add :role, :user_role
    end
  end
end
